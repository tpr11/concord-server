var config = require('../config');
var prompt = require('prompt');
var socket = require('socket.io-client')(`http://localhost:${config.serverPort}`);

var authenticated = false;

socket.on('identification-success', () => {
    authenticated = true;
    console.log('\nAuthenticated.');
});
socket.on('message', message => {
    console.log(`\n${message.username}: ${message.content} (${message.date})`);
});
socket.on('_error', error => {
    console.log(`\n${error}`);
});

socket.emit('identification', process.argv[3], process.argv[2]);

function main() {
    prompt.get('message', (err, res) => {
        if (authenticated) {
            socket.emit('message', res.message);
        }
        else {
            console.log('Not yet authenticated.');
        }
        return main();
    });
}

main();
