var jwt = require('jsonwebtoken');
var boom = require('boom');

var config = require('../config');
var validation = require('../util/validation');

module.exports = (server, model) => {  
    
    var io = require('socket.io')(server);
    
    io.on('connection', socket => {
        
        socket.on('identification', (roomId, token) => {
            jwt.verify(token, config.secret, (err, decoded) => {
                if (err) {
                    return socket.emit('_error', 'Internal server error');
                }
                model.User.findOne({_id: decoded.id}, (err, user) => {
                    if (err) {
                        return socket.emit('_error', 'Internal server error');
                    }
                    if (!user) {
                        return socket.emit('_error', 'User does not exist.');
                    }
                    model.Room.findOne({_id: roomId}, (err, room) => {
                        if (err) {
                            return socket.emit('_error', 'Internal server error');
                        }
                        for (let i = 0; i < room.users.length; i++) {
                            var userId = room.users[i];
                            if (userId.equals(decoded.id)) {
                                socket.user = {
                                    id: decoded.id,
                                    username: user.username,
                                    roomId: roomId
                                };
                                socket.join(roomId);
                                return socket.emit('identification-success');
                            }
                        }
                        return socket.emit('_error', 'Unauthorized');
                    });
                });
            });
        });

        socket.on('message', content => {
            if (!socket.user) {
                return socket.emit('_error', 'Room hasn\'t been accessed from API or user hasn\'t been identified.');
            }
            if (validation.validateMessage({content}).error) {
                return socket.emit('_error', 'Message does not meet guidelines.');
            }
            model.Message.create({
                user: socket.user.id,
                room: socket.user.roomId,
                content: content
            }, (err) => {
                if (err) {
                    return socket.emit('_error', 'Internal server error');
                }
                io.to(socket.user.roomId).emit('message', {
                    username: socket.user.username,
                    content: content,
                    date: Date.now()
                });
            });
        })
    });
};
