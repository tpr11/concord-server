var express = require('express');
var boom = require('boom');

var livechat = require('../live-chat/server');
var asyncMiddleware = require('../util/asyncMiddleware');
var validation = require('../util/validation');

module.exports = model => {

    router = express.Router();

    router.post('/rooms', asyncMiddleware(async (req, res) => {
        rooms = await model.Room
            .find({'users': req.userId}, '_id name creator')
            .populate('users', '-_id username')
            .populate('creator', '-_id username');
        res.status(200).json(rooms);
    }));

    router.post('/room', asyncMiddleware(async (req, res) => {
        room = await model.Room
            .findOne({_id: req.body.roomId}, '_id name creator')
            .populate('users', 'username')
            .populate('creator', '-_id username');
        if (!room) {
            throw boom.badRequest('Room does not exist.');
        }
        for(let i = 0; i < room.users.length; i++) {
            var userId = room.users[i]._id;
            if (userId.equals(req.userId)) {
                page = Math.floor(req.body.page ? req.body.page : 1);
                messages = await model.Message
                    .find({room: req.body.roomId}, '-_id user content date')
                    .skip((page-1) * 100)
                    .limit(100)
                    .sort({ date: -1 })
                    .populate('user', '-_id username');
                room = {
                    _id: room._id,
                    name: room.name,
                    users: room.users.map(user => ({username: user.username})),
                    creator: room.creator
                };
                return res.status(200).json(room);
            }
        }
        throw boom.unauthorized();
    }));

    router.post('/room/create', asyncMiddleware(async (req, res) => {
        if (validation.validateRoomCreation({name: req.body.name}).error) { 
            throw boom.badRequest('Room data does not respect guidelines.');
        }
        user = await model.User.findOne({_id: req.userId});
        var room = {
            name: req.body.name,
            users: [req.userId],
            creator: req.userId
        };
        room = await model.Room.create(room);
        res.status(200).json({
            roomId: room.id 
        });
    }));

    router.post('/room/changeName', asyncMiddleware(async (req, res) => {
        if (validation.validateRoomCreation({name: req.body.name}).error) { 
            throw boom.badRequest('Room data does not respect guidelines.');
        }
        await model.Room.update({_id: req.body.roomId}, {name: req.body.name});
        res.sendStatus(200);
    }));

    router.post('/room/delete', asyncMiddleware(async (req, res) => {
        room = await model.Room.findOne({_id: req.body.roomId});
        if (!room) {
            throw boom.badRequest('Room does not exist.');
        }
        if (!room.creator.equals(req.userId)) {
            throw boom.unauthorized();
        }
        await model.Room.deleteOne({_id: req.body.roomId});
        res.sendStatus(200);
    }));

    router.post('/room/add', asyncMiddleware(async (req, res) => {
        room = await model.Room.findOne({_id: req.body.roomId});
        if (!room) {
            throw boom.badRequest('Room does not exist.');
        }
        user = await model.User.findOne({username: req.body.username});
        if (!user) {
            throw boom.badRequest('User does not exist.');
        } 
        for (let i = 0; i < room.users.length; i++) {
            if (room.users[i].equals(user._id)) {
                throw boom.badRequest('User already in room.');   
            }
        }
        if (!room.creator.equals(req.userId)) {
            throw boom.badRequest('Only the creator of a room can add users to it.');
        }
        room.users.push(user._id);
        await model.Room.update({_id: req.body.roomId}, room);
        res.sendStatus(200);
    }));

    router.post('/room/leave', asyncMiddleware(async (req, res) => {
        room = await model.Room.findOne({_id: req.body.roomId});
        if (!room) {
            throw boom.badRequest('Room does not exist.');
        }
        for (let i = 0; i < room.users.length; i++) {
            var userId = room.users[i];
            if (userId.equals(req.userId)) {
                room.users = room.users.filter(user => !user.equals(req.userId));
                if (room.creator.equals(req.userId)) {
                    await model.Room.deleteOne({_id: req.body.roomId});
                }
                else {
                    await model.Room.update({_id: req.body.roomId}, room);
                }
                return res.sendStatus(200);
            }
        }
        throw boom.badRequest('User not in room.');
    }));

    return router;
}