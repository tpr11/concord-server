var express = require('express');
var bcrypt = require('bcrypt');
var boom = require('boom');

var asyncMiddleware = require('../util/asyncMiddleware');
var validation = require('../util/validation');

module.exports = model => {

    router = express.Router();

    router.post('/show', asyncMiddleware(async (req, res) => {
        user = await model.User.findOne({_id: req.userId});
        res.status(200).json({
            username: user.username,
            email: user.email
        });
    }));

    router.post('/update', asyncMiddleware(async (req, res) => {
        if (!req.body.passwordOld) {
            throw boom.badRequest('Missing password for verification.');
        }
        user = await model.User.findOne({_id: req.userId});
        same = await bcrypt.compare(req.body.passwordOld, user.password);
        if (!same) {
            throw boom.unauthorized();
        }
        if (req.body.passwordNew) {
            passwordNew = await bcrypt.hash(req.body.passwordNew, 10);
        }
        else {
            passwordNew = null;
        }
        var newUser = {
            username: req.body.username ? req.body.username : user.username,
            email: req.body.email ? req.body.email : user.email,
            password: passwordNew ? passwordNew : user.password,
        }
        if (validation.validateUserUpdate(newUser).error) {
            throw boom.badRequest('Credentials do not meet requirements.');
        }
        await model.User.update({_id: req.userId}, newUser);
        res.sendStatus(200);
    }));

    router.post('/delete', asyncMiddleware(async (req, res) => {
        await model.User.deleteOne({_id: req.userId});
        res.sendStatus(200);
    }));

    return router;
}