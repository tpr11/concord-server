var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var boom = require('boom');

var config = require('../config');
var asyncMiddleware = require('../util/asyncMiddleware');
var validation = require('../util/validation');

module.exports = (app, model) => {

    app.post('/register', asyncMiddleware(async (req, res) => {
        if (validation.validateUserCreation({
            username: req.body.username,
            email: req.body.email, 
            password: req.body.password
        }).error) {
            throw boom.badRequest('Credentials do not meet requirements.');
        } 
        user = await model.User.findOne({email: req.body.email});
        if (user) {
            throw boom.badRequest('User already exists.');
        }
        hash = await bcrypt.hash(req.body.password, 10);
        await model.User.create({
            username: req.body.username,
            email: req.body.email,
            password: hash
        });
        res.sendStatus(200);
    }));

    app.post('/login', asyncMiddleware(async (req, res, next) => {
        if (validation.validateUserLogin({
            email: req.body.email, 
            password: req.body.password
        }).error) {
            throw boom.badRequest('Credentials do not meet requirements.');
        } 
        user = await model.User.findOne({email: req.body.email});
        if (!user) {
            throw boom.badRequest('Wrong email or password.');
        }
        same = await bcrypt.compare(req.body.password, user.password);
        if (!same) {
            throw boom.badRequest('Wrong email or password.');
        }
        var token = jwt.sign({id: user._id}, config.secret, {expiresIn: 86400});
        res.status(200).send({
            token: token, 
            user: {
                username: user.username,
                email: user.email
            }
        });
    }));

    app.use('*', asyncMiddleware(async (req, res, next) => {
        if (!req.body.token) {
            throw boom.unauthorized();
        }
        decoded = await jwt.verify(req.body.token, config.secret);
        user = await model.User.findOne({_id: decoded.id});
        if (!user) {
            throw boom.unauthorized();
        }
        req.userId = decoded.id;
        next();
    }));
};