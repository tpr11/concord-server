var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var boom = require('boom');

var config = require('./config');
var CORSmiddleware = require('./util/CORSMiddleware');

var app = express();
var server = http.Server(app);
app.use(bodyParser.json());

mongoose.connect(config.databaseURL, { useNewUrlParser: true });
var model = require('./model/model')(mongoose);
require('./live-chat/server')(server, model);

app.use(CORSmiddleware);
require('./routing/authorization')(app, model);
app.use('/account', require('./routing/account')(model));
app.use('/chat', require('./routing/chat')(model));
app.use('*', (req, res) => {throw boom.notFound()})
app.use((err, req, res, next) => {
    if (err.isServer) {
        console.error(err);
    }
    res.status(err.output.statusCode).json({ error: err.output.payload });
})

server.listen(config.serverPort, () => console.log(`Express server listening on port ${config.serverPort}.`));