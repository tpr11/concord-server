module.exports = mongoose => {
    module.User = require('./User')(mongoose);
    module.Room = require('./Room')(mongoose);
    module.Message = require('./Message')(mongoose);
    return module;
};