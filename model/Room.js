module.exports = mongoose => {
    var RoomSchema = mongoose.Schema({
        name: {
            type: String,
            required: true
        },
        users: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User',
                required: true
            }
        ],
        creator: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        }
    });
    return mongoose.model('Room', RoomSchema);
};