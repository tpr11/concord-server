module.exports = mongoose => {
    var MessageSchema = mongoose.Schema({
        content: String,
        user: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        room: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Room'
        },
        date: {
            type: Date,
            default: Date.now
        }
    });
    return mongoose.model('Message', MessageSchema);
};