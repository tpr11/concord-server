module.exports = mongoose => {
    var UserSchema = mongoose.Schema({
        username: {
            type: String,
            unique: true,
            required: true
        },
        email: {
            type: String,
            unique: true,
            required: true
        },
        password: {
            type: String,
            required: true
        }
    });
    return mongoose.model('User', UserSchema);
};