var joi = require('joi');

var userCreateSchema = joi.object().keys({
    username: joi.string().alphanum().min(3).max(32).required(),
    email: joi.string().email().required(),
    password: joi.string().min(6).max(128).required()
});
function validateUserCreation (user) {
    return joi.validate(user, userCreateSchema);
}

var userLoginSchema = joi.object().keys({
    email: joi.string().email().required(),
    password: joi.string().min(6).max(128).required()
});
function validateUserLogin (user) {
    return joi.validate(user, userLoginSchema);
}

var userUpdateSchema = joi.object().keys({
    username: joi.string().alphanum().min(3).max(32),
    email: joi.string().email(),
    password: joi.string().min(6).max(128)
});
function validateUserUpdate (user) {
    return joi.validate(user, userUpdateSchema);
}

var roomCreateSchema = joi.object().keys({
    name: joi.string().min(3).max(128).required()
});
function validateRoomCreation (room) {
    return joi.validate(room, roomCreateSchema);
}

var messageSchema = joi.object().keys({
    content: joi.string().max(16384, 'utf-8')
});
function validateMessage (message) {
    return joi.validate(message, messageSchema);
}

module.exports = {
    validateUserCreation: validateUserCreation,
    validateUserLogin: validateUserLogin,
    validateUserUpdate: validateUserUpdate,
    validateRoomCreation: validateRoomCreation,
    validateMessage: validateMessage
};